<?php

namespace Drupal\ckeditor_remove_format\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to remove formats.
 *
 * @Filter(
 *   id = "filter_remove_format",
 *   title = @Translation("Remove Format Filter"),
 *   description = @Translation("Removes certain formats from CKEditor content."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class RemoveFormatFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $new_text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
    return new FilterProcessResult($new_text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Removes specific formats from content.');
  }

}
