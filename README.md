### CKEditor Remove Format

This module enhances CKEditor 5's Remove Format plugin by adding the ability to automatically remove format on save.
